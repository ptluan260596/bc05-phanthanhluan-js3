document.getElementById("btnKiemTra").onclick = function () {
  var canh1 = document.getElementById("canh1").value * 1;
  var canh2 = document.getElementById("canh2").value * 1;
  var canh3 = document.getElementById("canh3").value * 1;
  if (canh1 == canh2 && canh2 == canh3) {
    document.getElementById("ketQua").innerHTML = "Tam giác đều";
  } else if (canh1 == canh2 || canh2 == canh3 || canh1 == canh3) {
    document.getElementById("ketQua").innerHTML = "Tam giác cân";
  } else if (
    canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
    canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
    canh3 * canh3 == canh1 * canh1 + canh2 * canh2
  ) {
    document.getElementById("ketQua").innerHTML = "Tam giác vuông";
  } else {
    document.getElementById("ketQua").innerHTML = "Tam giác bình thường";
  }
};
